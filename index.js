const needle = require('needle');
const myRunner = require("./collectionRunner");
const sessions = require("./userSessions");
const RequestOrder = require('./requestOrder');
var uuid = require ('uuid');

var users = [
  "gilberte.francesca@conme.com",
  "alpha.zero@conme.com",
  "leela.zero@conme.com",
  "noel.jennifer@conme.com",
  "pandora.nonie@conme.com",
  "tobi.marlo@conme.com",
  "zarah.leonora@conme.com",
  "mariam.celesta@conme.com",
  "alica.shayna@conme.com",
  "brett.coretta@conme.com",
  "gladys.millie@conme.com",
  "diana.andreas@conme.com",
  "nonnah.darlleen@conme.com",
  "malory.hynda@conme.com",
  "birdie.carrie@conme.com",
  "caitlin.willie@conme.com",
  "marty.fredia@conme.com",
  "viviana.annalise@conme.com",
  "hephzibah.emelina@conme.com",
  "tania.ferdinanda@conme.com",
  "gabriell.davina@conme.com",
  "shanie.nissie@conme.com",
  "koralle.delphinia@conme.com",
  "vonnie.goldie@conme.com",
  "murielle.gail@conme.com",
  "nissy.madeleine@conme.com",
  "annabel.saba@conme.com",
  "effie.marga@conme.com"
]

let reactions = ["👍", "❤️", "😂", "😮", "😢", "🙏"]

// fetchConfigLinks();
// createAllSessions();
// fetchRooms("gilberte.francesca@conme.com")
react("annabel.saba@conme.com", "!FMGxoiUDIbuVWgRPHy:connectme-chat.eu-nordics-sto-development.dstny.d4sp.com", "👍")
// sendMany()

async function sendMany() {
  for (let i = 0; i < 14; i++) {
    let userId = users[i];
    const random = Math.floor(Math.random() * reactions.length);
    let reaction = reactions[random];
    await react(userId, "!FMGxoiUDIbuVWgRPHy:connectme-chat.eu-nordics-sto-development.dstny.d4sp.com", reaction)
  }
}

async function react(userId, roomId, reaction, eventId) {
  let session = await sessions.fetch(userId);
  if (!session) {
    console.log("Session not found")
    return;
  }

  
  let result = undefined

  if (eventId === undefined) {
    let merged = merge(session, { my_matrix_room_id: roomId });
    result = await myRunner.run(merged, [ "Room Messages" ]);
    let messages = JSON.parse(result["Room Messages"]);
    let latestEvent = messages.chunk.find(t => t.type == "m.room.message" && t.content.body)
    if (!latestEvent) {
      console.log(`No event to react`)
      return 
    }
    eventId = latestEvent.event_id;
    console.log(`Reacting to ${latestEvent.content.body}`)
  }
  var guid = uuid.v4();

  let merged = merge(session, { my_matrix_room_id: roomId, randomState: guid, event_id: eventId, reaction: reaction });
  result = await myRunner.run(merged, [ "React" ]);
  console.log(result)
}

async function fetchRooms(userId) {
  let session = await sessions.fetch(userId);
  if (!session) {
    console.log("Session not found")
    return;
  }
  let result = await myRunner.run(session, [ "Sync" ]);
  let responseString = result.Sync
  if (responseString) {
    let syncResponse = JSON.parse(responseString);

    console.log("")
    var rooms = `
================================================================
    Direct Rooms
================================================================
`
    let directRoomType = syncResponse.account_data.events.find(o => o.type === "m.direct");
    if (directRoomType) {
      let userIds = Object.keys(directRoomType.content)
      for (let userId of userIds) {
        let merged = merge(session, { my_matrix_user_id: userId });
        let result = await myRunner.run(merged, [ "Display Name" ]);
        let profile = JSON.parse(result["Display Name"]);
        rooms = rooms + `${profile.displayname}(${userId}) -> ${directRoomType.content[userId]}\n`;
      }
    }

    let syncRooms = syncResponse.rooms;
    let joined = syncRooms.join;
    let invited = syncRooms.invite;
    let leave = syncRooms.leave;
    rooms = rooms + `
================================================================
    Group rooms
================================================================
`
    for (let roomId of Object.keys(joined)) {
      let room = joined[roomId];
      let timeline = room.timeline.events;
      let latestEvent = timeline[0];
      let body = latestEvent.content.body;
      let timestamp = (new Date(latestEvent.origin_server_ts)).toLocaleString("sv");
      rooms = rooms + `${roomId} - ${latestEvent.type} - ${timestamp} - ${body}
`
    }

    console.log(rooms)
  }
}

function merge(object1, object2) {
  return {...object1, ...object2}
}