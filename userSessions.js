const fs = require('fs');
const runner = require("./collectionRunner");
const RequestOrder = require('./requestOrder');
const needle = require('needle');

class Session {
    constructor() {
        this.sessions = [];
        this.configURLs = [];
        this.#load();
    }

    saveSession(session) {
        if (session.configURL) {
            let index = this.sessions.findIndex(t => t.configURL == session.configURL)
            if (index === -1) {
                this.sessions.push(session);
            } else {
                this.sessions[index] = session;
            }
            this.#save()
        }
    }

    async fetchConfigLinks() {
        if (this.configURLs.length === 0) {
            console.log("Fetching config links")
            let username = "chat";
            let url = `https://e.yewo.io/pm/links?username=${username}`
            let response = await needle("get", url);
            this.configURLs = response.body;
        }
      }

    async fetch(userId) {
        let session = this.#getWithUserId(userId);
        if (session) {
            return session;
        }

        await this.fetchConfigLinks();
        let user = this.configURLs.find(o => o.name === userId);
        if (!user) {
            console.log("Couldn't find user")
            return undefined
        }

        session = this.#getWithConfigLink(user.link)
        if (session) {
            return session
        } else {
            await runner.run({ configURL: user.link }, RequestOrder.ACCESS_TOKEN);
            var variables = runner.collection.variables.members;
            session = { }

            for (let variable of variables) {
                session[variable.key] = variable.value
            }

            this.saveSession(session)
            return session;
        }
    }

    #getWithUserId(userId) {
        let index = this.sessions.findIndex(t => t.fullUserId == userId)
        if (index === -1) {
            return undefined
        } else {
            return this.sessions[index]
        }
    }

    #getWithConfigLink(configURL) {
        let index = this.sessions.findIndex(t => t.configURL == configURL)
        if (index === -1) {
            return undefined
        } else {
            return this.sessions[index]
        }
    }

    #save() {
        fs.writeFileSync("sessions.json", JSON.stringify(this.sessions));
    }

    #load() {
        if (fs.existsSync("sessions.json")) {
            let data = fs.readFileSync("sessions.json", "utf8");
            this.sessions = JSON.parse(data);
        }
    }
}

const session = new Session();
module.exports = session;