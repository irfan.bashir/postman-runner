const Collection = require("postman-collection").Collection
const runtime = require("postman-runtime")
const colors = require("colors");
const needle = require("needle");

class CollectionRunner {
    constructor() {
        this.runner = new runtime.Runner();
        this.collection = null;
        this.globals = { }
    };

    get(key) {
        let keyIndex = this.collection.variables.members.findIndex(t => t.key == key)
        if (keyIndex !== -1) {
            return this.collection.variables.members[keyIndex].value;
        }
        return undefined;
    }

    #set(key, value) {
        let keyIndex = this.collection.variables.members.findIndex(t => t.key == key)
        this.collection.variables.members[keyIndex].value = value;
    }

    async #fetchCollection() {
        if (this.collection == null) {
            let response = await needle("get", "https://gitlab.com/irfan.bashir/postman-collection/-/raw/main/dstny.mobile.postman_collection.json?inline=false");
            const collection = JSON.parse(response.body.toString());
            this.collection = new Collection(collection);
            console.log("collection fetched");
        }
    }

    async run(variables, order) {
        await this.#fetchCollection();

        console.log("Setting variables")
        for (const property in variables) {
            // console.log(`${property}: ${variables[property]}`);
            this.#set(property, variables[property]);
        }

        this.result = { }
      
        let time = Date.now();
        for (let request of order) {
            await this.#run(request);
        }
    
        var diffMs = Date.now() - time;
        var diffSeconds = Math.round(diffMs / 1000);
    
        console.log("");
        // console.log(`${(new Date()).toJSON()}: Completed in ${(diffSeconds + "")}s`);
        return this.result
    }

    #run(name) {
        return new Promise((resolve, reject) => {
          this.runner.run(
            this.collection,
            {
              stopOnError: false,
      
              // - abruptly halts the run on errors, and directly calls the `done` callback
              abortOnError: false,
      
              // - gracefully halts on errors _or_ test failures.
              //   calls the `item` and `iteration` callbacks and does not run any further items (requests)
              stopOnFailure: false,
      
              // - abruptly halts the run on errors or test failures, and directly calls the `done` callback
              abortOnFailure: false,
              globals: this.globals,
              entrypoint: {
                execute: name,
              },
              fileResolver: require("fs"),
            },
            (err, run) => {
              // Check the section below for detailed documentation on what callbacks should be.
              run.start({
                  beforeRequest: (err, cursor, request, item) => {
                  console.log(`${(new Date()).toJSON()}: ---- ${item.name} ----`)
                  console.log(`${(new Date()).toJSON()}: ${request.url.toString()}`)
                },
                test: (err, cursor, results, item) => {
                  this.collection.variables =
                    results[results.length - 1].result.collectionVariables.values;
                },
                item: (err, cursor, item, visualizer, result) => {
                },
                responseStart: (err, cursor, response, request, item, cookies, history) => {
                  // console.log(`${(new Date()).toJSON()}: Response started`)
                },
                response: (
                  err,
                  cursor,
                  response,
                  request,
                  item,
                  cookies,
                  history
                ) => {
                  console.log(`${(new Date()).toJSON()}: ${response.code} ${response.status}: ${ err == null ? "Completed" : err } in ${this.formatTime(response.responseTime)} size: ${this.formatBytes(response.responseSize, 2)}`);
                  if (response.code !== 200) {
                      console.log(response.stream.toString())
                  }
                  //resolve(response.stream.toString());
                  this.result[name] = response.stream.toString();
                },
                done: (err) => {
                  if (err) {
                    reject(err);
                  } else {
                    resolve()
                  }
                },
              });
            }
          );
        });
    }

    formatTime(time) {
        if (time < 50) {
          return `${time}ms`.green;
        } else if (time < 100) {
          return `${time}ms`.brightGreen;
        } else if (time < 150) {
          return `${time}ms`.yellow;
        } else if (time < 300) {
          return `${time}ms`.orange;
        } else {
          return `${time}ms`.red;
        }
    }
      
    formatBytes(bytes, decimals = 2) {
        if (!+bytes) return "0 Bytes";
      
        const k = 1024;
        const dm = decimals < 0 ? 0 : decimals;
        const sizes = [
          "Bytes",
          "KiB",
          "MiB",
          "GiB",
          "TiB",
          "PiB",
          "EiB",
          "ZiB",
          "YiB",
        ];
      
        const i = Math.floor(Math.log(bytes) / Math.log(k));
      
        return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
    }
}

const Runner = new CollectionRunner();

module.exports = Runner;