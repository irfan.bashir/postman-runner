module.exports = Object.freeze({
    ACCESS_TOKEN:   [
      "Mobile Configuration",
      "GET JWT",
      "Service Discovery",
      "Login with JWT"
    ],
    REACT:  [
      "Mobile Configuration",
      "GET JWT",
      "Service Discovery",
      "Login with JWT",
      "React",
    ],
 });
  